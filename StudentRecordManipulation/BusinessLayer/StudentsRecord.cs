﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using StudentRecordManipulation.MapperClass;
using StudentRecordManipulation.DataRepo;
using StudentRecordManipulation.Interface;

namespace StudentRecordManipulation.BusinessLayer
{
    public class StudentsRecord : IStudentsGenerator
    {
        public string Name { get; set; }
        public string Id { get; set; }

        public List<DataMapper> GetAllStudent()
        {
            StudentRepo StdRepo = new StudentRepo();

            List<StudentRepo> StdList = StdRepo.GetStudentList();
            List<DataMapper> stdList = new List<DataMapper>();

            for (int i = 0; i < StdList.Count; i++)
            {
                stdList.Add(new DataMapper { FName = StdList[i].Name, MId = StdList[i].Id });
            }
            return stdList;
        }

        public void AddStudents(DataMapper PostData)
        {
            StudentRepo.stdListData.Add(new StudentRepo { Name = PostData.FName, Id = PostData.MId });
        }

        public void DeleteStudent(string Index)
        {
            StudentRepo.stdListData.RemoveAll(student => student.Id == Index);
        }
    }
}