﻿
/// <reference path="../angular.min.js" />
/// <reference path="angular-route.min.js" />


// Modules
var HomeModule = angular.module("HomeModule", ['ngRoute', 'UpdateModule', 'LookUpModule', 'RegisterModule', 'DeleteModule']);
var UpdateModule = angular.module("UpdateModule", [])
var LookUpModule = angular.module("LookUpModule", []);
var RegisterModule = angular.module("RegisterModule", []);
var DeleteModule = angular.module("DeleteModule", []);


// Config
HomeModule.config(function ($routeProvider, $locationProvider) {
    $routeProvider.
          when("/Home", { templateUrl: "Scripts/views/Home.html" })
          .when("/Delete", { templateUrl: "Scripts/views/Delete.html" })
           .when("/Register", { templateUrl: "Scripts/views/Register.html" })
            .when("/Upadate", { templateUrl: "Scripts/views/Upadate.html" })
            .when("/LookUp", { templateUrl: "Scripts/views/LookUp.html" })
          .otherwise({
              redirectTo: "/Home"
          })
    $locationProvider.html5Mode(true);
})


// Factory 
HomeModule.factory('FactoryMethods', function () {

    var GetStudent = function () {

        $http.get('api/students')
 .then(
     function (response) {
         var stdLst = response.data;
         $scope.stdLst = stdLst;
         console.log($scope.stdLst);
     },
     function (response) {
         alert("Connection Error");
     }
  );

    };
});


// Controllers
HomeModule.controller("HomeController", function ($scope, $http) {
 
    $scope.OnLookUpClick = function () {
        window.location = "#/LookUp"
    }

    $scope.OnRegisterClick = function () {
        window.location = "#/Register"
    }

    $scope.OnUpdateClick = function () {
        window.location = "#/Upadate"
    }

    $scope.OnDeleteClick = function () {
        window.location = "#/Delete"
    }
    
});

UpdateModule.controller("UpdateController", function ($scope, $http) {

    $http.get('api/students')
   .then(
       function (response) {
           var stdLst = response.data;
           $scope.stdLst = stdLst;
           console.log($scope.stdLst);
       },
       function (response) {
           alert("Connection Error");
       }
    );

    $scope.btnEditStd = function (std) {
        std.Id = std.mId;
        alert($scope.std.Name);
    }
    
});

LookUpModule.controller("LookUpController", function ($scope, $http) {

    $http.get('api/students')
   .then(
       function (response) {
           var stdLst = response.data;
           $scope.stdLst = stdLst;
           console.log($scope.stdLst);
       },
       function (response) {
           alert("Connection Error");
       }
    );

});

RegisterModule.controller("RegisterController", function ($scope, $http) {

    $http.get('api/students')
   .then(
       function (response) {
           var stdLst = response.data;
           $scope.stdLst = stdLst;
           console.log($scope.stdLst);
       },
       function (response) {
           alert("Connection Error");
       }
    );

    $scope.btnAddStd = function () {
        var data = { FName: $scope.std.Name, MId: $scope.std.Id };
        $http.post('api/students', angular.toJson(data));
    }

});

DeleteModule.controller("Deleteontroller", function ($scope, $http) {
   
    $http.get('api/students')
  .then(
      function (response) {
          var stdLst = response.data;
          $scope.stdLst = stdLst;
          console.log($scope.stdLst);
      },
      function (response) {
          alert("Connection Error");
      }
   );

    $scope.btnDeleteStd = function (std) {
        var index = std.mId;
        $http.delete('api/students/'+ index, index);
       
    }
});



 