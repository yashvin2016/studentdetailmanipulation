﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using StudentRecordManipulation.MapperClass;
using StudentRecordManipulation.BusinessLayer;

namespace StudentRecordManipulation.Controllers
{
    public class StudentsController : ApiController
    {
        // GET api/students
        public IEnumerable<DataMapper> Get()
        {
            StudentsRecord StdRecord = new StudentsRecord();
            List<DataMapper> ListOfStd = StdRecord.GetAllStudent();
            return ListOfStd;
        }

        // GET api/students/5
        public string Get(int id)
        {
            return "value";
        }

        // POST api/students
        [HttpPost]
        public void Post([FromBody]DataMapper PostData)
        {
            StudentsRecord StdRecord = new StudentsRecord();
            StdRecord.AddStudents(PostData);
        }

        // PUT api/students/5
        [HttpPut]
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/students/5
        [HttpDelete]
        public void Delete(int Id)
        {
            StudentsRecord StdRecord = new StudentsRecord();
            StdRecord.DeleteStudent(Id.ToString());
        }
    }
}
