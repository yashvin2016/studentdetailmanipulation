﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using StudentRecordManipulation.MapperClass;

namespace StudentRecordManipulation.Interface
{
    interface IStudentsGenerator
    {
        List<DataMapper> GetAllStudent();
        void AddStudents(DataMapper PostData);
        void DeleteStudent(string Index);
        
    }
}
