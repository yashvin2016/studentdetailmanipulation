﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StudentRecordManipulation.DataRepo
{
    public class StudentRepo
    {
        public static List<StudentRepo> stdListData = new List<StudentRepo>();
        public string Name { get; set; }
        public string Id { get; set; }

        public List<StudentRepo> GetStudentList()
        {
            stdListData.Add(new StudentRepo { Name = "Yashvin", Id = "1" });
            stdListData.Add(new StudentRepo { Name = "Sumo", Id = "2" });
            stdListData.Add(new StudentRepo { Name = "BMW", Id = "3" });
            stdListData.Add(new StudentRepo { Name = "Husky", Id = "4" });

            return stdListData;
        }

    }
}